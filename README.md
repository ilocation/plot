#plot4ol3

### 说明

基于OpenLayers3 (http://openlayers.org) 实现动态标绘API。

在线体验 http://ilocation.oschina.io/plot4ol3sample

demo源码 http://git.oschina.net/ilocation/plot4ol3sample

当前版本V1.0，实现的功能包括：
- 标绘符号绘制；
- 标绘符号编辑；
- 实现的线状符号：弧线、曲线、折线、自由线；
- 实现的面状符号：圆、椭圆、弓形、扇形、曲线面、集结地、多边形、自由面；
- 实现的箭头符号：钳击、直箭头、细直箭头、突击方向、进攻方向、进攻方向（燕尾）、分队战斗行动、分队战斗行动（燕尾）；

### 演示截图
<img src="http://git.oschina.net/uploads/images/2016/0312/094123_6d5a9b97_642920.jpeg" width=640 height=640>